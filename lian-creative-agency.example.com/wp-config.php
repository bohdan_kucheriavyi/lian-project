<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('WP_CACHE', true);
define( 'WPCACHEHOME', 'E:\xampp\htdocs\lian-creative-agency.example.com\wp-content\plugins\wp-super-cache/' );
define('DB_NAME', 'wp_lian');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'YGqX9^2I)}U,}zU.L6FB]nQ[0?f)9rz:$C^BGj8Rb@=oF&d%@?NtF@eY9l#3QeHS');
define('SECURE_AUTH_KEY',  '1K!wFJf^X@U-?rk?NL?Nnw^dN<%D$kKeErBFX=S+.Q&K3, W]e>;&4X]N%V>@$Q,');
define('LOGGED_IN_KEY',    's=yo(@T!H]xH3>w.q8dboC] Ygo6uz~66RR4}kfD;(yO&h<orlh6(#uM;lS}Yu$z');
define('NONCE_KEY',        'mQAB CbIr%,-g7d`<}0hUthxD}lI$UA1$I^8#Nl@~Z9SXb-Q[)JVNxz,OqC@pLQS');
define('AUTH_SALT',        '=a^J(i6a^u1?052/tNPNF0F$Cs;mEz$3t+6wn4ix=|$.LXO5#@SG~Ja!1~Zj ^ o');
define('SECURE_AUTH_SALT', 'Dmz%.Pz,QC[|0x-DP8Z^@Im+tza[U{7m)C<C0@S!OyVbN)J2zM/6.GBBYR,CEs>b');
define('LOGGED_IN_SALT',   'vTOgT~(]tvfj9/ ^e+EO{G6@(mhBt<NN4kMl<ueA`zt:6{<7Jzcbb(aU8~SXiB?n');
define('NONCE_SALT',       '%+j&BN`bL:sH@Z.8X]bg#/U=glQ2(j!c2n&IVgB7EJ]BiDx_Qd}KcQN+d[&bg7hL');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');

<?php
/**
 * Template Name: Template Contact
 */
get_header(); ?>
<?php get_template_part( 'template-parts/inner-header'); ?>
<section class="contact_page">
    <div class="container">
        <div class="wrapper fx">
            <div class="contacts wow fadeInLeft">
                <h2 class="page_title"><?php _e("OFFICE INFO", 'wplian')?></h2>
                <?php if(have_posts()) : while (have_posts()): the_post(); ?>
                    <div class="page_content story">
                        <?php the_content(); ?>
                    </div>
                <?php endwhile; endif; ?>
                <hr class="line">
                <div class="contacts_data">
                    <div class="third_part">
                        <span class="contacts_title"><?php _e('Office Hours: ', 'wplian') ?></span>
                        <?php echo the_field('officehours'); ?>
                    </div>
                    <div class="third_part">
                        <span class="contacts_title"><?php _e('Address: ', 'wplian') ?></span>
                        <?php echo the_field('address'); ?>
                    </div>
                    <div class="third_part">
                        <div class="contact_phone">
                            <span class="contacts_title"><?php _e('Tell: ', 'wplian') ?></span>
                            <a href="tel:<?php echo the_field('tell'); ?>"><?php echo the_field('tell'); ?></a><br>
                        </div>
                        <div class="contact_phone">
                            <span class="contacts_title"><?php _e('Fax: ', 'wplian') ?></span>
                            <a href="tel:<?php echo the_field('fax'); ?>"><?php echo the_field('fax'); ?></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="contacts_form wow fadeInRight">
                <?php echo do_shortcode('[contact-form-7 id="197" title="Contact Form"]'); ?>
            </div>
        </div>
    </div>
    <div class="contact_map">
        <?php if(get_field('address')){
            echo do_shortcode('[map address="'.get_field('address').'" width="100%" height="360px"]');
        }?>
    </div>
</section>
<section class="skills_section">
    <div class="wrapper wow fadeInUp">
        <h2 class="page_title"><?php _e('OUR VALUES', 'wplian') ?></h2>
        <div class="contact_values">
            <p><?php echo get_field('description'); ?></p>
        </div>
        <div class="contact_skills cf">
            <?php $staff = CFS()->get( 'values' );
            if($staff){
                foreach($staff as $field){
                    echo '<div class="skills_item"><img src="'. $field['values_image'] .'" alt="val_img">
                            <h3 class="item_title">' . $field['values_title'] . '</h3>
                            <p class="item_desc">' . $field['values_desc'] . '</p></div>';
                }
            } ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>

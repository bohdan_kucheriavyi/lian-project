<?php
show_admin_bar(false);

//**************
//LANG
//**************

function wplian_setup(){
    load_theme_textdomain('wplian', get_template_directory() . '/lang');
}
add_action('after_setup_theme', 'wplian_setup');

//**************
//CONNECTING
//CSS
//AND
//SCRIPTS
//**************

require get_template_directory() . '/inc/enqueue/conn-css-and-scripts.php';

//**************
//ADD
//THEME
//CUSTOMIZATION
//MENU
//**************

function add_theme_menu_item() {
    add_theme_page("Theme Customization", "Theme Customization", "manage_options", "theme-options", "theme_option_page", null, 99);

}
add_action('admin_menu', 'add_theme_menu_item');

//**************
//OPTIONS
//FOR
//THEME
//MENU
//**************

require get_template_directory() . '/inc/custom-options.php';

//**************
//REGISTER
//NAVIGATION
//MENUS
//**************

require get_template_directory() . '/inc/nav-menus.php';

//**************
//REGISTER
//SHORTCODES
//MAP
//**************

require get_template_directory() . '/inc/shortcodes/map-shortcode.php';

//**************
//ADD
//IMAGE
//SIZE
//**************

require get_template_directory() . '/inc/add-image-size.php';

//**************
//CUSTOM
//POST
//TYPE
//**************

require get_template_directory() . '/inc/custom-types.php';

//**************
//AJAX
//*************

require get_template_directory() . '/inc/ajax/loadmore-posts.php';

require get_template_directory() . '/inc/ajax/like-system.php';

require get_template_directory() . '/inc/ajax/load-filter.php';

//**************
//WIDGETS
//*************

require get_template_directory() . '/inc/widgets/right-sidebar.php';


<?php echo get_search_form(); ?>
<?php dynamic_sidebar('Right sidebar'); ?>

<div class="widget_item">
    <h3 class="widget_title"><?php _e('Instagram', 'wplian') ?></h3>
    <?php echo do_shortcode('[instagram-feed]'); ?>
</div>
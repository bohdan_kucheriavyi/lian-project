<?php
/**
 * Template Name: Template About
 */
get_header(); ?>
<?php get_template_part( 'template-parts/inner-header'); ?>
    <section class="about_page">
        <div class="container">
            <div class="wrapper">
                <div class="about_section cf">
                    <div class="photo_section">
                        <?php if(have_posts()) : while(have_posts()) : the_post();?>
                        <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>
                        <img src="<?php echo $url ?>" alt="photo"/>
                    </div>
                    <div class="content_section cf">
                        <h2 class="page_title"><?php _e("Our Story", 'wplian')?></h2>
                        <div class="page_content story">
                            <?php the_content(); ?>
                        </div>
                        <h2 class="page_title"><?php _e("Our skills", 'wplian')?></h2>
                        <div class="skills_data">
                            <?php $staff = CFS()->get( 'ourskills' );
                            if($staff){
                                foreach($staff as $field){
                                    echo '  <div class="skills_bar" data-percent="'. $field['skill_value'] . '%">
                                                <h3 class="progress-title">' . $field['skill_title'] . '</h3>
                                                <div class="progress">
                                                <div class="progress-bar">
                                                <div class="progress-value">' . $field['skill_value'] . '%
                                                </div>
                                                </div>
                                                </div>
                                                </div>';
                                }
                            } ?>
                        </div>
                    </div>
                    <?php endwhile; endif; ?>
                </div>
                <div class="skills_section person_section wow fadeInUp">
                    <h2 class="page_title"><?php _e('Awesome Team', 'wplian') ?></h2>
                    <div class="contact_values">
                        <p><?php echo get_field('description_team'); ?></p>
                    </div>
                    <div class="contact_skills cf">
                        <?php $staff = CFS()->get( 'awesome_team' );
                        if($staff){
                            foreach($staff as $field){
                                echo '<div class="skills_item"><img src="'. $field['photo_person'] .'" alt="person_img"><div class="person_item">
                            <h3 class="item_title">' . $field['name_person'] . '</h3>
                            <p class="item_position">'. $field['position_job'].'</p>
                            <p class="item_desc">' . $field['description_person'] . '</p></div></div>';
                            }
                        } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>


<?php get_footer(); ?>
<?php get_header(); ?>
<?php get_template_part( 'template-parts/blog-inner-header'); ?>
    <section class="blog_single_page">
        <div class="container">
            <div class="wrapper">
                <?php if(have_posts()): while (have_posts()): the_post(); ?>
                    <h2 class="post_title"><?php the_title(); ?></h2>
                    <div class="post_info cf">
                        <div class="post_author">
                            <?php _e('by ', 'wplian'); the_author(); ?>
                        </div>
                        <div class="post_date">
                            <?php echo get_the_date(); ?>
                        </div>
                        <div class="my-post-like post_like" data-id="<?php the_ID(); ?>">
                            <span class="like-count"><?php display_post_likes( get_the_ID() );  ?></span><?php _e(' likes', 'wplian'); ?>
                        </div>
                    </div>
                    <div class="post_image wow fadeIn">
                        <?php $featured_img_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'large'); ?>
                        <img src="<?php echo $featured_img_url[0]; ?>" alt="post_image">
                    </div>
                    <div class="post_content story">
                        <?php the_content(); ?>
                    </div>
                <?php endwhile; endif; ?>
                <?php comments_template(); ?>
            </div>
        </div>
    </section>

<?php get_footer(); ?>
<?php get_header(); ?>
<?php get_template_part( 'template-parts/inner-header'); ?>
    <section class="single_portfolio">
        <?php get_template_part( 'template-parts/single-post/pagination'); ?>
        <div class="container">
            <div class="wrapper">
                <?php if(have_posts()): while (have_posts()) : the_post(); ?>
                    <div class="currentpost_section cf">
                        <?php get_template_part( 'template-parts/single-post/slider'); ?>
                        <div class="currentpost_info">
                            <h2 class="post_title"><?php the_title(); ?></h2>
                            <?php get_template_part( 'template-parts/single-post/info'); ?>
                            <div class="post_share cf">
                                <span class="share_text"><?php _e('Share','wplian'); ?></span>
                                <?php echo do_shortcode('[ssba-buttons]') ?>
                            </div>
                        </div>
                    </div>
                <?php endwhile; endif; ?>
                <div class="skills_section related_section wow fadeInUp">
                    <div class="wrapper">
                        <?php
                        $tags = wp_get_post_terms( get_queried_object_id(), 'tag', ['fields' => 'ids'] );
                        $args = [
                            'post__not_in'        => array( get_queried_object_id() ),
                            'posts_per_page'      => 3,
                            'ignore_sticky_posts' => 1,
                            'orderby'             => 'rand',
                            'tax_query' => [
                                [
                                    'taxonomy' => 'tag',
                                    'terms'    => $tags
                                ]
                            ]
                        ];
                        $my_query = new wp_query( $args );
                        if( $my_query->have_posts() ) { ?>
                        <h2 class="page_title"><?php _e('Related Works', 'wplian'); ?></h2>
                        <div class="contact_values">
                            <p><?php echo get_field('related_works'); ?></p>
                        </div>
                        <div class="contact_skills cf">
                        <?php while( $my_query->have_posts() ) {
                            $my_query->the_post();
                            $featured_img_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full'); ?>
                                <div id="post-<?php the_ID(); ?>" class="skills_item" style="background-image: url(<?php echo $featured_img_url[0]; ?>)">
                                    <a href="<?php the_permalink();?>">
                                        <span class="mask">
                                            <span class="plus">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </span>
                                        </span>
                                    </a>
                                </div>
                        <?php } ?>
                        </div><!--contact_skills-->
                        <?php wp_reset_postdata();?>
                    </div>
                </div>
            <?php } ?>
            </div>
        </div>
        </div>
    </section>
<?php get_footer(); ?>
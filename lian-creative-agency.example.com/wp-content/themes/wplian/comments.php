<?php
// Do not delete these lines for security reasons
if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME'])) {
    die ('Please do not load this page directly. Thanks!');
}
?>

<h3 class="comment_title"><?php _e('Leave a Comment', 'wplian') ?></h3>
<?php echo do_shortcode('[contact-form-7 id="345" title="Leave a comment"]'); ?>

<div class="category_page">
    <ul class="categorymenu cf filter">
        <li class="active"><a href="/portfolio/">All</a></li>
        <?php $terms = get_terms('portfolio_category', array('hide_empty' => 0, 'parent' => 0));
        foreach ($terms as $term) : ?>

            <li><a href="<?php echo get_term_link( $term->slug, $term->taxonomy ); ?>" data-slug="<?php echo $term->slug;?>"><?php echo $term->name;?></a></li>

        <?php endforeach; ?>
    </ul>
</div>
<?php
$args = array(
    'posts_per_page' => 10,
    'post_type' => 'portfolio',
);
$portfolio = new WP_Query($args);
$max_pages = $portfolio->max_num_pages;
$i = 0;
if($portfolio->have_posts()) : ?>
    <div class="portfolio_posts_page wow fadeInUp">
        <div class="grid cf">
            <?php while($portfolio->have_posts()): $portfolio->the_post();?>
                        <?php $featured_img_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
                        if($i == 1 or $i == 4 or $i == 5 or $i == 7) {?>
                           <article id="post-<?php the_ID(); ?>" class="portfolio_post grid-item grid-item--height2" style="background-image: url(<?php echo $featured_img_url[0]; ?>)">
                               <a href="<?php the_permalink();?>">
                                   <span class="mask">
                                        <span class="plus">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                        </span>
                                   </span>
                               </a>
                           </article>
                        <?php }elseif ($i == 2 or $i == 6){ ?>
                            <article id="post-<?php the_ID(); ?>" class="portfolio_post grid-item grid-item--height3" style="background-image: url(<?php echo $featured_img_url[0]; ?>)">
                                <a href="<?php the_permalink();?>">
                                    <span class="mask">
                                        <span class="plus">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                        </span>
                                    </span>
                                </a>
                            </article>
                        <?php }else{ ?>
                            <article id="post-<?php the_ID(); ?>" class="portfolio_post grid-item" style="background-image: url(<?php echo $featured_img_url[0]; ?>)">
                                <a href="<?php the_permalink();?>">
                                    <span class="mask">
                                        <span class="plus">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                        </span>
                                    </span>
                                </a>
                            </article>
                        <?php }$i++ ?>
            <?php endwhile;?>
        </div>
    </div>
        <script>
            var ajaxurl = '<?php echo site_url() ?>/wp-admin/admin-ajax.php';
            var true_posts = '<?php echo serialize($wp_query->query_vars); ?>';
            var current_page = <?php echo (get_query_var('paged')) ? get_query_var('paged') : 1; ?>;
            var max_pages = '<?php echo $max_pages; ?>';
        </script>
        <div id="true_loadmore"><?php _e('Load More', 'wplian') ?></div>

    <?php wp_reset_postdata();
endif;?>

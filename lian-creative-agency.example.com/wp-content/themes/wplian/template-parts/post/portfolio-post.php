<?php $featured_img_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
if($i == 1 or $i == 4 or $i == 5 or $i == 7) {?>
    <article id="post-<?php the_ID(); ?>" class="portfolio_post grid-item grid-item--height2" style="background-image: url(<?php echo $featured_img_url[0]; ?>)">
        <a href="<?php the_permalink();?>">
                                   <span class="mask">
                                        <span class="plus">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                        </span>
                                   </span>
        </a>
    </article>
<?php }elseif ($i == 2 or $i == 6){ ?>
    <article id="post-<?php the_ID(); ?>" class="portfolio_post grid-item grid-item--height3" style="background-image: url(<?php echo $featured_img_url[0]; ?>)">
        <a href="<?php the_permalink();?>">
                                    <span class="mask">
                                        <span class="plus">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                        </span>
                                    </span>
        </a>
    </article>
<?php }else{ ?>
    <article id="post-<?php the_ID(); ?>" class="portfolio_post grid-item" style="background-image: url(<?php echo $featured_img_url[0]; ?>)">
        <a href="<?php the_permalink();?>">
                                    <span class="mask">
                                        <span class="plus">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                        </span>
                                    </span>
        </a>
    </article>
<?php }$i++ ?>
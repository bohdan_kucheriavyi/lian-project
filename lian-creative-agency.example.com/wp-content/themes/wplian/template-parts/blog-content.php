<?php $featured_img_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
 foreach((get_the_category()) as $category) { ?>
    <?php if($i == 1 or $i == 2 or $i == 4) {?>
        <article id="post-<?php the_ID(); ?>" class="item_blog grid-item grid-item--height3">
            <a href="<?php the_permalink(); ?>">
                <img class="big_post_image" src="<?php echo $featured_img_url[0]; ?>" alt="big_post_image">
                <div class="post_info">
                    <h3 class="post_title"><?php the_title(); ?></h3>
                    <p class="post_author"><?php _e('by ', 'wplian'); the_author(); ?></p>
                    <?php $text = get_the_content();
                    $content = wp_trim_words( $text, 20, '...' );
                    echo '<p class="post_desc story">'.$content.'</p>'; ?>
                    <p class="post_date"><?php _e('on ', 'wplian'); echo get_the_date(); ?></p>
                    <div class="post_social">
                        <a href="#"><i class="fa fa-share-alt" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-heart" aria-hidden="true"></i></a>
                    </div>
                </div>
            </a>
            <span class="blog_category">
                                            <?php echo $category->cat_name; ?>
                                    </span>
        </article>
    <?php } else if($i == 3) {?>
        <article id="post-<?php the_ID(); ?>" class="item_blog grid-item grid-item--height2" style="background-image: url(<?php echo $featured_img_url[0]; ?>)">
            <a href="<?php the_permalink();?>">
                                            <span class="mask">
                                                <span class="play">
                                                    <i class="fa fa-play" aria-hidden="true"></i>
                                                </span>
                                            </span>
            </a>
        </article>
    <?php } else {?>
        <article id="post-<?php the_ID(); ?>" class="item_blog grid-item blog-medium">
            <a href="<?php the_permalink(); ?>">
                <div class="post_info">
                    <p class="blog_category"><?php echo $category->cat_name; ?></p>
                    <h3 class="post_title"><?php the_title(); ?></h3>
                    <p class="post_author"><?php _e('by ', 'wplian'); the_author(); ?></p>
                    <?php $text = get_the_content();
                    $content = wp_trim_words( $text, 20, '...' );
                    echo '<p class="post_desc story">'.$content.'</p>'; ?>
                    <p class="post_date"><?php _e('on ', 'wplian'); echo get_the_date(); ?></p>
                    <div class="post_social">
                        <a href="#"><i class="fa fa-share-alt" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-heart" aria-hidden="true"></i></a>
                    </div>
                </div>
            </a>
        </article>
    <?php } $i++;?>
<?php }  ?>
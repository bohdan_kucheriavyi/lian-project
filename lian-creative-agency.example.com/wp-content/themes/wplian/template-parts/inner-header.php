<?php if(!is_page_template('page-home.php')){ ?>
    <section class="inner_header">
        <?php $image = get_field('image');
        $archimage = get_option('my_header_photo');
        if (!empty($image) or !empty($archimage)): ?>
        <div class="header_image" style="background-image: url(<?php if(is_post_type_archive('portfolio') or is_singular( 'portfolio' ) or is_tax('portfolio_category')) echo $archimage; else echo $image['url']; ?>);">
            <?php endif; ?>
            <div class="wrapper">
                <div class="header_text wow fadeIn">
                    <span class="main_title"><?php if(is_post_type_archive('portfolio') or is_singular( 'portfolio' ) or is_tax('portfolio_category')) echo get_option('my_header_title'); else the_field('title'); ?></span><br />
                    <span class="main_subtitle"><?php if(is_post_type_archive('portfolio') or is_singular( 'portfolio' ) or is_tax('portfolio_category')) echo get_option('my_header_subtitle'); else the_field('subtitle'); ?></span>
                </div>
            </div>
        </div>
    </section>
<?php } ?>
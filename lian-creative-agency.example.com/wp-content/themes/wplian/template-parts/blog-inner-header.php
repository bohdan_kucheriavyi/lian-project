<section class="inner_header">
    <?php $archimage = get_option('my_header_post_photo');
    if (!empty($image)) ?>
    <div class="header_image" style="background-image: url(<?php echo $archimage; ?>);">
        <div class="wrapper">
            <div class="header_text wow fadeIn">
                <span class="main_title"><?php echo get_option('my_header_post_title'); ?></span><br />
                <span class="main_subtitle"><?php echo get_option('my_header_post_subtitle'); ?></span>
            </div>
        </div>
    </div>
</section>
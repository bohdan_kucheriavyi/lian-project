<div class="slider">
    <div id="slider" class="flexslider portfolioslider">
        <ul class="slides">
            <?php $featured_img_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full'); ?>
            <li class="slides_thumb" style="background-image: url(<?php echo $featured_img_url[0]; ?>)">
            </li>
            <?php $staff = CFS()->get( 'post_images' );
            if($staff){
                foreach($staff as $field){
                    echo '<li class="slides_thumb" style="background-image: url('. $field['post_image'] .')"></li>';
                }
            } ?>
        </ul>
    </div>
    <div id="carousel" class="flexslider portfoliocarousel">
        <ul class="slides">
            <li class="slides_carousel" style="background-image: url(<?php echo $featured_img_url[0]; ?>)">
            </li>
            <?php $staff = CFS()->get( 'post_images' );
            if($staff){
                foreach($staff as $field){
                    echo '<li class="slides_carousel" style="background-image: url('. $field['post_image'] .')"></li>';
                }
            } ?>
        </ul>
    </div>
</div>
<div class="category_name">
    <?php
    $term_list = wp_get_post_terms($post->ID, 'portfolio_category', array("fields" => "all"));
    echo $term_list[0]->name ;
    _e(' Design', 'wplian');
    ?>
</div>
<div class="post_info">
    <div class="post_date">
        <?php echo get_the_date(); ?>
    </div>
    <div class="my-post-like post_like" data-id="<?php the_ID(); ?>">
        <span class="like-count"><?php display_post_likes( get_the_ID() );  ?></span><?php _e(' likes', 'wplian'); ?>
    </div>
</div>
<div class="post_content story">
    <?php the_content(); ?>
</div>
<?php if (get_field('post_role')) {?>
    <div class="post_role">
        <h3 class="post_subtitle"><?php _e('Role on project:', 'wplian'); ?></h3>
        <?php echo the_field('post_role'); ?>
    </div>
<?php } ?>
<?php $term_list = wp_get_post_terms($post->ID, 'tag', array("fields" => "all"));
 if ($term_list) {?>
<div class="post_tags">
    <h3 class="post_subtitle"><?php _e('Tags:', 'wplian'); ?></h3>

    <?php foreach($term_list as $term_single) {
        echo $term_single->name.', ';
    }
    $output = ob_get_clean();
    echo rtrim($output, ', ');
    ?>
</div>
<?php } ?>
<div class="single_menu cf">
    <div class="wrapper">
        <div class="pagination">
            <div class="arrows left_arrow">
                <?php previous_post_link('%link', '<i class="fa fa-angle-left" aria-hidden="true"></i> Previous', false); ?>
            </div>
            <div class="arrows right_arrow">
                <?php next_post_link('%link', 'Next <i class="fa fa-angle-right" aria-hidden="true"></i>', false); ?>
            </div>
        </div>
        <div class="all_posts_button">
            <a href="<?php echo home_url(); ?>/portfolio"><i class="fa fa-list-ul" aria-hidden="true"></i><?php _e('Show All', 'wplian'); ?></a>
        </div>
    </div>
</div>
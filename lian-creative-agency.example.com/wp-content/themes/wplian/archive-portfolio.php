<?php get_header(); ?>
<?php get_template_part( 'template-parts/inner-header'); ?>
    <section class="contact_page portfolio_page">
        <div class="container">
            <div class="wrapper">
                <div class="page_content gallery_page cf">
                    <?php get_template_part( 'template-parts/post/filter'); ?>
                    <?php get_template_part( 'template-parts/post/content'); ?>
                </div>
            </div>
        </div>
    </section>
<?php get_footer(); ?>
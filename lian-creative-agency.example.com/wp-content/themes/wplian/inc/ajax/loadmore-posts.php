<?php

function true_load_posts(){

    $args = unserialize( stripslashes( $_POST['query'] ) );
    $args['paged'] = $_POST['page'] + 1;
    $args['post_status'] = 'publish';

    query_posts( $args );

    if( have_posts() ) : ?>
        <div class="portfolio_posts_page">
            <div class="grid">
                <?php  while( have_posts() ): the_post();
                    if(is_post_type_archive( 'portfolio' ))
                        get_template_part( 'template-parts/post/portfolio-post');
                    else
                        get_template_part('template-parts/blog-content');
                endwhile; ?>
            </div>
        </div>
    <?php endif;
    wp_die();
}


add_action('wp_ajax_loadmore', 'true_load_posts');
add_action('wp_ajax_nopriv_loadmore', 'true_load_posts');
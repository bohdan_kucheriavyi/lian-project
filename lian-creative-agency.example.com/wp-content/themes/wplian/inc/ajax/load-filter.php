<?php
add_action( 'wp_ajax_get_cat', 'ajax_show_posts_in_cat' );
add_action( 'wp_ajax_nopriv_get_cat', 'ajax_show_posts_in_cat' );
function ajax_show_posts_in_cat() {

    $slug = ! empty( $_POST['slug'] ) ? esc_attr( $_POST['slug'] ) : false;
    $cat = get_term_by('slug', $slug, 'portfolio_category');

    $args = array(
        'posts_per_page' => 10,
        'post_status'    => 'publish',
        'post_type' => 'portfolio',
        'tax_query' => array(
            array(
                'taxonomy' => 'portfolio_category',
                'field'    => 'term_id',
                'terms'    => $cat->term_id,
            )
        )
    );

    query_posts( $args );
    $current_term = single_term_title('', false);?>

     <div class="page_content gallery_page cf">
                    <div class="category_page">
                        <ul class="categorymenu cf filter">
                            <li><a href="/portfolio/">All</a></li>
                            <?php $terms = get_terms('portfolio_category', array('hide_empty' => 0, 'parent' => 0));
                            foreach ($terms as $term) : ?>

                                <li <?php if($current_term == $term->name) echo 'class="active"'; ?>><a data-slug="<?php echo $term->slug;?>" href="<?php echo get_term_link( $term->slug, $term->taxonomy ); ?>"><?php echo $term->name;?></a></li>

                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <?php
                    $i = 0;
                    if(have_posts()) : ?>
                        <div class="portfolio_posts_page">
                            <div class="grid cf">
                                <?php while(have_posts()): the_post();?>
                                    <?php $featured_img_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
                                    if($i == 1 or $i == 4 or $i == 5 or $i == 7) {?>
                                        <article id="post-<?php the_ID(); ?>" class="portfolio_post grid-item grid-item--height2" style="background-image: url(<?php echo $featured_img_url[0]; ?>)">
                                            <a href="<?php the_permalink();?>">
                                   <span class="mask">
                                        <span class="plus">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                        </span>
                                   </span>
                                            </a>
                                        </article>
                                    <?php }elseif ($i == 2 or $i == 6){ ?>
                                        <article id="post-<?php the_ID(); ?>" class="portfolio_post grid-item grid-item--height3" style="background-image: url(<?php echo $featured_img_url[0]; ?>)">
                                            <a href="<?php the_permalink();?>">
                                    <span class="mask">
                                        <span class="plus">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                        </span>
                                    </span>
                                            </a>
                                        </article>
                                    <?php }else{ ?>
                                        <article id="post-<?php the_ID(); ?>" class="portfolio_post grid-item" style="background-image: url(<?php echo $featured_img_url[0]; ?>)">
                                            <a href="<?php the_permalink();?>">
                                    <span class="mask">
                                        <span class="plus">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                        </span>
                                    </span>
                                            </a>
                                        </article>
                                    <?php }$i++ ?>
                                <?php endwhile;?>
                            </div>
                            <?php if (  $wp_query->max_num_pages > 1 ) : ?>
                                <script>
                                    var ajaxurl = '<?php echo site_url() ?>/wp-admin/admin-ajax.php';
                                    var true_posts = '<?php echo serialize($wp_query->query_vars); ?>';
                                    var current_page = <?php echo (get_query_var('paged')) ? get_query_var('paged') : 1; ?>;
                                    var max_pages = '<?php echo $wp_query->max_num_pages; ?>';
                                </script>
                                <div id="true_loadmore"><?php _e('Load More', 'wplian') ?></div>
                            <?php endif; ?>
                        </div>

                        <?php wp_reset_postdata();
                    endif;?>

                </div>

  <?php  wp_die();
}

add_action( 'wp_enqueue_scripts', 'my_assets' );
function my_assets() {
    wp_enqueue_script( 'true_loadmore', plugins_url( 'ajax-scripts.js', __FILE__ ), array( 'jquery' ) );

    wp_localize_script( 'true_loadmore', 'myPlugin', array(
        'ajaxurl' => admin_url( 'admin-ajax.php' )
    ) );
}
<?php

function right_widgets_init() {

    register_sidebar( array(
        'name' => 'Right sidebar',
        'id' => 'right_1',
        'before_widget' => '<div class="widget_item">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget_title">',
        'after_title' => '</h3>',
    ) );
}
add_action( 'widgets_init', 'right_widgets_init' );
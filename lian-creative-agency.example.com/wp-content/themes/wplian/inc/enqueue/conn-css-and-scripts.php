<?php

function lian_css() {
    wp_enqueue_style( 'style', get_template_directory_uri() . '/css/style.css', 'style' );
    wp_enqueue_style( 'fonts', get_template_directory_uri() . '/css/fonts.css', 'style' );
    wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', 'style' );
    wp_enqueue_style( 'animate', get_template_directory_uri() . '/css/animate.css', 'style' );
}
add_action( 'wp_enqueue_scripts', 'lian_css' );

function lian_scripts() {

    //all pages
    wp_deregister_script('jquery');
    wp_register_script('jquery', get_template_directory_uri() . '/js/jquery-3.3.1.min.js');
    wp_enqueue_script('jquery');

    wp_register_script('wow', get_template_directory_uri() . '/js/wow.min.js' );
    wp_enqueue_script( 'wow','','','',true );

    wp_register_script('custom-scripts', get_template_directory_uri() . '/js/custom-scripts.js' );
    wp_enqueue_script( 'custom-scripts','','','',true  );

    wp_enqueue_script( 'true_loadmore', get_stylesheet_directory_uri() . '/js/ajax-scripts.js', array('jquery'), time(), true);

    //portfolio, blog page, home page, taxonomy portfolio category, search page
    if(is_home() or is_page_template('page-home.php') or is_post_type_archive('portfolio') or is_tax('portfolio_category') or is_search()){
    wp_deregister_script('masonry');
    wp_register_script('masonry', get_template_directory_uri() . '/js/masonry.pkgd.min.js' );
    wp_enqueue_script( 'masonry', '','','',true);

    wp_register_script('masonry-scripts', get_template_directory_uri() . '/js/masonry-scripts.js' );
    wp_enqueue_script( 'masonry-scripts', '','','',true );
    }

    //single portfolio
    if (is_singular('portfolio')){
    wp_register_script('modules', get_template_directory_uri() . '/js/modules.js' );
    wp_enqueue_script( 'modules','','','',true  );

    wp_register_script('scripts', get_template_directory_uri() . '/js/scripts.js' );
    wp_enqueue_script( 'scripts','','','',true  );
    }

}
add_action( 'wp_enqueue_scripts', 'lian_scripts' );
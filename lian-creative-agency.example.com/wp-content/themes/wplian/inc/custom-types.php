<?php

//**************
//TAXONOMI
//**************

add_action( 'init', 'portfolio_category', 0 );
function portfolio_category() {
    $args = array(
        'label' => _x( 'Category', 'taxonomy general name' ),
        'labels' => array(
            'name' => _x( 'Category', 'taxonomy general name' ),
            'singular_name' => _x( 'Category', 'taxonomy singular name' ),
            'menu_name' => __( 'Category' ),
            'all_items' => __( 'All Category' ),
            'edit_item' => __( 'Edit Category' ),
            'view_item' => __( 'View Category' ),
            'update_item' => __( 'Update Category' ),
            'add_new_item' => __( 'Add New Category' ),
            'new_item_name' => __( 'Name' ),
            'parent_item' => __( 'Parent' ),
            'parent_item_colon' => __( 'Parent:' ),
            'search_items' => __( 'Search Category' ),
            'popular_items' => null,
            'separate_items_with_commas' => null,
            'add_or_remove_items' => null,
            'choose_from_most_used' => null,
            'not_found' => __( 'Category Not Found.' ),
        ),
        'public' => true,
        'show_ui' => true,
        'show_tagcloud' => true,
        'show_in_quick_edit' => true,
        'hierarchical' => true,
        'query_var' => true,
        'sort' => null,
        '_builtin' => false,
    );
    register_taxonomy( 'portfolio_category', array('portfolio'), $args );
}

add_action( 'init', 'create_tag_taxonomies', 0 );

//create two taxonomies, genres and tags for the post type "tag"
function create_tag_taxonomies()
{
    // Add new taxonomy, NOT hierarchical (like tags)
    $labels = array(
        'name' => _x( 'Tags', 'taxonomy general name' ),
        'singular_name' => _x( 'Tag', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Tags' ),
        'popular_items' => __( 'Popular Tags' ),
        'all_items' => __( 'All Tags' ),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __( 'Edit Tag' ),
        'update_item' => __( 'Update Tag' ),
        'add_new_item' => __( 'Add New Tag' ),
        'new_item_name' => __( 'New Tag Name' ),
        'separate_items_with_commas' => __( 'Separate tags with commas' ),
        'add_or_remove_items' => __( 'Add or remove tags' ),
        'choose_from_most_used' => __( 'Choose from the most used tags' ),
        'menu_name' => __( 'Tags' ),
    );

    register_taxonomy('tag','portfolio',array(
        'hierarchical' => false,
        'labels' => $labels,
        'show_ui' => true,
        'update_count_callback' => '_update_post_term_count',
        'query_var' => true,
        'rewrite' => array( 'slug' => 'tag' ),
    ));
}

//**************
//CUSTOM
//POST
//TYPE
//**************

add_action( 'init', 'register_post_portfolio', 0 );
function register_post_portfolio() {
    $args = array(
        'label'  => _x( 'Portfolio', 'Post Type General Name', 'text_domain' ),
        'labels' => array(
            'name' => _x( 'Portfolio', 'Post Type General Name', 'text_domain' ),
            'singular_name' => _x( 'Portfolio', 'Post Type Singular Name', 'text_domain' ),
        ),
        'public' => true,
        'menu_position' => 5,
        'has_archive' => true,
        'show_ui' => true,
        'taxonomies' => array('portfolio_category', 'tag'),
        'supports' => array(
            'title',
            'editor',
            'thumbnail',
        ),
        'show_in_nav_menus' => true,
    );
    register_post_type( 'portfolio', $args );
}

add_filter( 'manage_portfolio_posts_columns', 'set_custom_edit_portfolio_columns' );
function set_custom_edit_portfolio_columns($columns) {
    $columns['tag'] = __( 'Tags', 'your_text_domain' );
    $columns['category'] = __( 'Category', 'your_text_domain' );
    return $columns;
}

// Add the data to the custom columns for the book post type:
add_action( 'manage_portfolio_posts_custom_column' , 'custom_portfolio_column', 10, 2 );
function custom_portfolio_column( $column, $post_id ) {
    switch ( $column ) {

        case 'category' :
            $terms = get_the_term_list( $post_id , 'portfolio_category' , '' , ',' , '' );
            if ( is_string( $terms ) )
                echo $terms;
            break;

        case 'tag' :
            $terms = get_the_term_list( $post_id , 'tag' , '' , ',' , '' );
            if ( is_string( $terms ) )
                echo $terms;
            break;

        case 'publisher' :
            echo get_post_meta( $post_id , 'publisher' , true );
            break;

    }
}

function add_submenu_portfolio(){
    add_submenu_page(
        'edit.php?post_type=portfolio',
        'Header Settings',
        'Settings',
        'manage_options',
        'portfolio_settings',
        'portfolio_header_settings_page'
    );
}
add_action( 'admin_menu', 'add_submenu_portfolio');

function portfolio_header_settings_page(){
    ?>
    <div class="wrap">
        <h1>Theme Customization</h1>
        <form method="post" action="options.php">
            <?php
            settings_fields("header-options-grp");
            do_settings_sections("header-options");
            submit_button();
            ?>
        </form>
    </div>
    <?php
}

function theme_header_description(){
    echo '<p>Header site settings.</p>';
}

function header_settings()
{
    add_option('first_field_option', 1);
    add_settings_section('header_section', 'Header Options',
        'theme_header_description', 'header-options');

    add_settings_field('header_title', 'Title', 'header_title_desc', 'header-options', 'header_section');
    register_setting( 'header-options-grp', 'my_header_title');

    add_settings_field('header_subtitle', 'Subtitle', 'header_subtitle_desc', 'header-options', 'header_section');
    register_setting( 'header-options-grp', 'my_header_subtitle');

    add_settings_field("header_photo", "Background Url", "header_photo_desc", "header-options", "header_section");
    register_setting( 'header-options-grp', 'my_header_photo');
}
add_action('admin_init','header_settings');

function header_title_desc(){ ?>
    <input size="75px" type="text" name="my_header_title" id="my_header_title" value="<?php echo get_option('my_header_title'); ?>" /><?php
}

function header_subtitle_desc(){ ?>
    <input size="75px" type="text" name="my_header_subtitle" id="my_header_subtitle" value="<?php echo get_option('my_header_subtitle'); ?>" /><?php
}

function header_photo_desc()
{ ?>
    <input size="75px" type="text" name="my_header_photo" id="my_header_photo" value="<?php echo get_option('my_header_photo'); ?>" />
    <a href="http://localhost/wp-admin/media-new.php" class="page-title-action">Add New</a>
    <br><br>
    <img src="<?php echo get_option('my_header_photo'); ?>" alt="logo" style="max-width: 250px;"> <?php
}

//*********
//POST TYPE

function add_submenu_post(){
    add_submenu_page(
        'edit.php',
        'Header Settings',
        'Settings',
        'manage_options',
        'post_settings',
        'post_header_settings_page'
    );
}
add_action( 'admin_menu', 'add_submenu_post');

function post_header_settings_page(){
    ?>
    <div class="wrap">
        <h1>Theme Customization</h1>
        <form method="post" action="options.php">
            <?php
            settings_fields("post-header-options-grp");
            do_settings_sections("post-header-options");
            submit_button();
            ?>
        </form>
    </div>
    <?php
}

function theme_header_post_description(){
    echo '<p>Header site settings.</p>';
}

function header_post_settings()
{
    add_option('first_field_option', 1);
    add_settings_section('header_post_section', 'Header Options',
        'theme_header_post_description', 'post-header-options');

    add_settings_field('header_post_title', 'Title', 'header_post_title_desc', 'post-header-options', 'header_post_section');
    register_setting( 'post-header-options-grp', 'my_header_post_title');

    add_settings_field('header_post_subtitle', 'Subtitle', 'header_post_subtitle_desc', 'post-header-options', 'header_post_section');
    register_setting( 'post-header-options-grp', 'my_header_post_subtitle');

    add_settings_field("header_post_photo", "Background Url", "header_post_photo_desc", "post-header-options", "header_post_section");
    register_setting( 'post-header-options-grp', 'my_header_post_photo');
}
add_action('admin_init','header_post_settings');

function header_post_title_desc(){ ?>
    <input size="75px" type="text" name="my_header_post_title" id="my_header_post_title" value="<?php echo get_option('my_header_post_title'); ?>" /><?php
}

function header_post_subtitle_desc(){ ?>
    <input size="75px" type="text" name="my_header_post_subtitle" id="my_header_post_subtitle" value="<?php echo get_option('my_header_post_subtitle'); ?>" /><?php
}

function header_post_photo_desc()
{ ?>
    <input size="75px" type="text" name="my_header_post_photo" id="my_header_post_photo" value="<?php echo get_option('my_header_post_photo'); ?>" />
    <a href="http://localhost/wp-admin/media-new.php" class="page-title-action">Add New</a>
    <br><br>
    <img src="<?php echo get_option('my_header_post_photo'); ?>" alt="logo" style="max-width: 250px;"> <?php
}

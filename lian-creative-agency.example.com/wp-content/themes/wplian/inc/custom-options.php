<?php
function theme_option_page() {
    ?>
    <div class="wrap">
        <h1>Theme Customization</h1>
        <form method="post" action="options.php">
            <?php
            settings_fields("theme-options-grp");
            do_settings_sections("theme-options");
            submit_button();
            ?>
        </form>
    </div>
    <?php
}

//**************
//DESCRIPTION
//AND
//TITLE
//FOR
//MENU
//**************

function theme_section_description(){
    echo '<p>Basic site settings.</p>';
}

function theme_section_description_social(){
    echo '<p>Paste a link to the social networks you need and save.</p>';
}

function theme_section_description_map(){
    echo '<p>Map settings</p>';
}

function theme_settings(){
    add_option('first_field_option',1);
    add_settings_section( 'basic_section', 'Basic Options',
        'theme_section_description','theme-options');

    add_option('first_field_option',1);
    add_settings_section( 'social_section', 'Social Networks',
        'theme_section_description_social','theme-options');

    add_option('first_field_option',1);
    add_settings_section( 'map_section', 'Map Options',
        'theme_section_description_map','theme-options');

//**************
//OPTIONS
//FOR
//THEME
//MENU
//**************
//SOCIAL
//**************

    add_settings_field('facebook_url', 'Facebook Profile Url', 'display_facebook_element', 'theme-options', 'social_section');
    register_setting( 'theme-options-grp', 'my_facebook_url');

    add_settings_field('google_plus_url', 'Google Plus Profile Url', 'display_google_plus_element', 'theme-options', 'social_section');
    register_setting( 'theme-options-grp', 'my_google_plus_url');

    add_settings_field('twitter_url', 'Twitter Profile Url', 'display_twitter_element', 'theme-options', 'social_section');
    register_setting( 'theme-options-grp', 'my_twitter_url');

    add_settings_field('linkedin_url', 'LinkedIn Profile Url', 'display_linkedin_element', 'theme-options', 'social_section');
    register_setting( 'theme-options-grp', 'my_linkedin_url');

    add_settings_field('dribbble_url', 'Dribbble Profile Url', 'display_dribbble_element', 'theme-options', 'social_section');
    register_setting( 'theme-options-grp', 'my_dribbble_url');

    add_settings_field('pinterest_url', 'Pinterest Profile Url', 'display_pinterest_element', 'theme-options', 'social_section');
    register_setting( 'theme-options-grp', 'my_pinterest_url');

    add_settings_field('instagram_url', 'Instagram Profile Url', 'display_instagram_element', 'theme-options', 'social_section');
    register_setting( 'theme-options-grp', 'my_instagram_url');

//**************
//BASIC
//**************

    add_settings_field("favicon", "Icon Title Url", "favicon_display", "theme-options", "basic_section");
    register_setting( 'theme-options-grp', 'my_favicon');

    add_settings_field("logo_header", "Logo HEADER Url", "logo_header_display", "theme-options", "basic_section");
    register_setting( 'theme-options-grp', 'my_logo_header');

    add_settings_field("logo_footer", "Logo FOOTER Url", "logo_footer_display", "theme-options", "basic_section");
    register_setting( 'theme-options-grp', 'my_logo_footer');

    add_settings_field("slider", "Slider Shortcode", "slider_display", "theme-options", "basic_section");
    register_setting( 'theme-options-grp', 'my_slider');

    add_settings_field("copyrights", "Copyrights Text", "copyrights_display", "theme-options", "basic_section");
    register_setting( 'theme-options-grp', 'my_copyrights');

//**************
//MAP
//**************

    add_settings_field("mapicon_url", "Map ICON URL", "display_mapicon_element", "theme-options", "map_section");
    register_setting( 'theme-options-grp', 'my_mapicon_url');

    add_settings_field("mapstyle", "Map STYLE", "display_mapstyle_element", "theme-options", "map_section");
    register_setting( 'theme-options-grp', 'my_mapstyle');
}
add_action('admin_init','theme_settings');

//**************
//HTML
//CODE
//FOR
//OPTIONS
//**************
//SOCIAL
//**************

function display_facebook_element(){ ?>
    <input size="75px" type="text" name="my_facebook_url" id="my_facebook_url" value="<?php echo get_option('my_facebook_url'); ?>" /><?php
}

function display_google_plus_element(){ ?>
    <input size="75px" type="text" name="my_google_plus_url" id="my_google_plus_url" value="<?php echo get_option('my_google_plus_url'); ?>" /><?php
}

function display_twitter_element(){ ?>
    <input size="75px" type="text" name="my_twitter_url" id="my_twitter_url" value="<?php echo get_option('my_twitter_url'); ?>" /> <?php
}

function display_linkedin_element(){ ?>
    <input size="75px" type="text" name="my_linkedin_url" id="my_linkedin_url" value="<?php echo get_option('my_linkedin_url'); ?>" /><?php
}

function display_dribbble_element(){ ?>
    <input size="75px" type="text" name="my_dribbble_url" id="my_dribbble_url" value="<?php echo get_option('my_dribbble_url'); ?>" /><?php
}

function display_pinterest_element(){ ?>
    <input size="75px" type="text" name="my_pinterest_url" id="my_pinterest_url" value="<?php echo get_option('my_pinterest_url'); ?>" /><?php
}

function display_instagram_element(){ ?>
    <input size="75px" type="text" name="my_instagram_url" id="my_instagram_url" value="<?php echo get_option('my_instagram_url'); ?>" /> <?php
}

//**************
//BASIC
//**************

function favicon_display()
{ ?>
    <input size="75px" type="text" name="my_favicon" id="my_favicon" value="<?php echo get_option('my_favicon'); ?>" />
    <a href="http://localhost/wp-admin/media-new.php" class="page-title-action">Add New</a>
    <br><br>
    <img src="<?php echo get_option('my_favicon'); ?>" alt="logo" style="max-width: 250px;"> <?php
}

function logo_header_display()
{ ?>
    <input size="75px" type="text" name="my_logo_header" id="my_logo_header" value="<?php echo get_option('my_logo_header'); ?>" />
    <a href="http://localhost/wp-admin/media-new.php" class="page-title-action">Add New</a>
    <br><br>
    <img src="<?php echo get_option('my_logo_header'); ?>" alt="logo" style="max-width: 250px;"> <?php
}

function logo_footer_display()
{ ?>
    <input size="75px" type="text" name="my_logo_footer" id="my_logo_footer" value="<?php echo get_option('my_logo_footer'); ?>" />
    <a href="http://localhost/wp-admin/media-new.php" class="page-title-action">Add New</a>
    <br><br>
    <img src="<?php echo get_option('my_logo_footer'); ?>" alt="logo" style="max-width: 250px;"> <?php
}

function slider_display()
{ ?>
    <input size="75px" type="text" name="my_slider" id="my_slider" value="<?php echo get_option('my_slider'); ?>" /> <?php
}

function copyrights_display()
{ ?>
    <textarea rows="10" cols="77" type="text" name="my_copyrights" id="my_copyrights"><?php echo get_option('my_copyrights'); ?></textarea> <?php
}

//**************
//MAP
//**************

function display_mapicon_element()
{ ?>
    <input size="75px" type="text" name="my_mapicon_url" id="my_mapicon_url" value="<?php echo get_option('my_mapicon_url'); ?>" />
    <a href="http://localhost/wp-admin/media-new.php" class="page-title-action">Add New</a>
    <br><br>
    <img src="<?php echo get_option('my_mapicon_url'); ?>" alt="logo" style="max-width: 250px;"> <?php
}

function display_mapstyle_element()
{ ?>
    <textarea rows="10" cols="77" type="text" name="my_mapstyle" id="my_mapstyle"><?php echo get_option('my_mapstyle'); ?></textarea> <?php
}
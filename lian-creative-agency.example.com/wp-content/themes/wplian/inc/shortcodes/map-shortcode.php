<?php
if (!function_exists('map')) {
    function map( $atts, $content = null ) {
        extract(shortcode_atts(array(
            'address'   => false,
            'widht'  => '100%',
            'height'    => '400px',
        ), $atts));

        $address = $atts['address'];

        if( $address ) :

            wp_print_scripts( 'google-maps-api' );

            $coordinates = map_get_coordinates( $address );

            if( !is_array( $coordinates ) )
                return;

            $map_id = uniqid( 'map_' );

            ob_start(); ?>
            <div class="map_canvas" id="<?php echo esc_attr( $map_id ); ?>" style="height: <?php echo esc_attr( $atts['height'] ); ?>; width: <?php echo esc_attr( $atts['width'] ); ?>"></div>
            <script type="text/javascript">
                var map_<?php echo $map_id; ?>;
                function run_map_<?php echo $map_id ; ?>(){
                    var location = new google.maps.LatLng("<?php echo $coordinates['lat']; ?>", "<?php echo $coordinates['lng']; ?>");
                    var map_options = {
                        zoom: 15,
                        center: location,
                        styles: <?php echo get_option('my_mapstyle'); ?>,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    }
                    map_<?php echo $map_id ; ?> = new google.maps.Map(document.getElementById("<?php echo $map_id ; ?>"), map_options);
                    var marker = new google.maps.Marker({
                        position: location,
                        map: map_<?php echo $map_id ; ?>,
                        icon: "<?php echo get_option('my_mapicon_url'); ?>",
                    });
                }
                run_map_<?php echo $map_id ; ?>();
            </script>
        <?php
        endif;
        return ob_get_clean();

    }
    add_shortcode('map', 'map');


    //Loads Google Map API
    function map_load_scripts() {
        wp_register_script( 'google-maps-api', 'http://maps.google.com/maps/api/js?sensor=false' );
    }
    add_action( 'wp_enqueue_scripts', 'map_load_scripts' );


    //Retrieve coordinates for an address
    function map_get_coordinates( $address, $force_refresh = false ) {

        $address_hash = md5( $address );

        $coordinates = get_transient( $address_hash );

        if ($force_refresh || $coordinates === false) {

            $args       = array( 'address' => urlencode( $address ), 'sensor' => 'false' );
            $url        = add_query_arg( $args, 'http://maps.googleapis.com/maps/api/geocode/json' );
            $response 	= wp_remote_get( $url );

            if( is_wp_error( $response ) )
                return;

            $data = wp_remote_retrieve_body( $response );

            if( is_wp_error( $data ) )
                return;

            if ( $response['response']['code'] == 200 ) {

                $data = json_decode( $data );

                if ( $data->status === 'OK' ) {

                    $coordinates = $data->results[0]->geometry->location;

                    $cache_value['lat'] 	= $coordinates->lat;
                    $cache_value['lng'] 	= $coordinates->lng;
                    $cache_value['address'] = (string) $data->results[0]->formatted_address;

                    // cache coordinates for 3 months
                    set_transient($address_hash, $cache_value, 3600*24*30*3);
                    $data = $cache_value;

                } elseif ( $data->status === 'ZERO_RESULTS' ) {
                    return __( 'No location for the address.', 'wplian' );
                } elseif( $data->status === 'INVALID_REQUEST' ) {
                    return __( 'Bad request. Did you enter an address name?', 'wplian' );
                } else {
                    return __( 'Error, please check if you have entered the shortcode correctly.', 'wplian' );
                }

            } else {
                return __( 'Can\'t connect Google API.', 'wplian' );
            }

        } else {
            // return cached results
            $data = $coordinates;
        }

        return $data;
    }


    //Fix bug with responsive
    function map_css() {
        echo '<style type="text/css">
            .map_canvas img {
                max-width: none;
          }</style>';
    }
    add_action( 'wp_head', 'map_css' );
}

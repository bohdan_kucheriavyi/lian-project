<?php

function wordpress_setup() {
    add_theme_support( 'post-thumbnails' );
    add_image_size( 'portfolio-little', 350, 250, true );
    add_image_size( 'portfolio-square', 350, 350, true );
    add_image_size( 'portfolio-big', 350, 450, true );
}
add_action('after_setup_theme', 'wordpress_setup');
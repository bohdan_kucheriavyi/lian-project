<?php
function init_navigation(){
    if(function_exists('register_nav_menus')){
        register_nav_menus(array(
            'header_menu' => __('Header Menu', 'wplian'),
            'footer_menu' => __('Footer Menu', 'wplian'),
            'mobile_menu' => __('Mobile Menu', 'wplian'),
        ));
    }
}
add_action('init', 'init_navigation');
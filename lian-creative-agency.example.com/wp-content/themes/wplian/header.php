<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]></--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <title><?php wp_title('|', true, 'right'); bloginfo('name'); ?></title>
    <link rel="shortcut icon" href="<?php echo get_option('my_favicon'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?> >
    <header class="top_header" id="navbar">
        <div class="wrapper cf">
            <div class="header_third">
                <div class="logo_section">
                    <?php if(get_option('my_logo_header')){?>
                        <a href="<?php echo home_url("/"); ?>" class="logo_link">
                            <img src="<?php echo get_option('my_logo_header'); ?>" alt="logo">
                        </a>
                    <?php } ?>
                </div>
            </div>
            <div class="header_third menu_section">
                <nav class="top_navigation">
                    <?php
                    if(has_nav_menu( 'header_menu') ){
                        wp_nav_menu(array(
                            'theme_location' =>  'header_menu',
                            'menu'          =>  'Header Menu',
                            'menu_class'    =>  'headermenu cf',
                            'container'     =>  '',
                        ));
                    }
                    ?>
                </nav>
                <div class="search_box">
                    <?php echo get_search_form(); ?>
                </div>
            </div>
            <div class="toggle_menu">
                <i class="fa fa-bars menu_icon" aria-hidden="true"></i>
            </div>
        </div>
        <div class="header_third res_menu_section cf">
            <nav class="top_navigation">
                <?php
                if(has_nav_menu( 'header_menu') ){
                    wp_nav_menu(array(
                        'theme_location' =>  'header_menu',
                        'menu'          =>  'Header Menu',
                        'menu_class'    =>  'headermenu cf',
                        'container'     =>  '',
                    ));
                }
                ?>
            </nav>
            <div class="search_box">
                <?php echo get_search_form(); ?>
            </div>
        </div>
    </header>

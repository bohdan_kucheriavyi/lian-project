<?php get_header(); ?>
<section class="inner_header">
    <div class="header_image" style="background-image: url('http://images.tskr.co.uk/404-Background.jpg');">
        <div class="wrapper">
            <div class="header_text wow fadeIn">
                <span class="main_title"><?php _e('HTTP 404', 'wplian'); ?></span><br />
                <span class="main_subtitle"><?php _e('not Found', 'wplian'); ?></span>
            </div>
        </div>
    </div>
</section>
<section class="blog_single_page">
    <div class="container">
        <div class="wrapper">
            <h2 class="post_title"><?php _e( 'Page Not Found', 'wplian' ); ?></h2>
            <div class="post_info">
            <p><?php _e( 'It looks like nothing was found at this location.', 'wplian' ); ?></p>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>

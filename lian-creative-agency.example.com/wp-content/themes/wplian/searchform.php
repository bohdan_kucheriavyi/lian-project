<form role="search" action="/" method="get" id="searchform">
    <fieldset>
        <input type="text" name="s" id="s" class="searchinput" placeholder="Search" maxlength="100" value="<?php the_search_query(); ?>" />
        <input type="submit" id="searchsubmit" class="headerfont" value="&#xf002">
    </fieldset>
</form>
<footer class="main_footer">
    <div class="footer_logo">
        <div class="wrapper">
            <div class="footer_inner">
                <?php if(get_option('my_logo_footer')){?>
                    <a href="<?php echo home_url("/"); ?>" class="logo_link">
                        <img class="" src="<?php echo get_option('my_logo_footer'); ?>" alt="footer_logo">
                    </a>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="footer_social wow zoomIn">
        <?php if (get_option('my_facebook_url')){?><a href="<?php echo get_option('my_facebook_url'); ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a><?php } ?>
        <?php if (get_option('my_google_plus_url')){?><a href="<?php echo get_option('my_google_plus_url'); ?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a><?php } ?>
        <?php if (get_option('my_twitter_url')){?><a href="<?php echo get_option('my_twitter_url'); ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a><?php } ?>
        <?php if (get_option('my_linkedin_url')){?><a href="<?php echo get_option('my_linkedin_url'); ?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a><?php } ?>
        <?php if (get_option('my_dribbble_url')){?><a href="<?php echo get_option('my_dribbble_url'); ?>"><i class="fa fa-dribbble" aria-hidden="true"></i></a><?php } ?>
        <?php if (get_option('my_pinterest_url')){?><a href="<?php echo get_option('my_pinterest_url'); ?>"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a><?php } ?>
        <?php if (get_option('my_instagram_url')){?><a href="<?php echo get_option('my_instagram_url'); ?>"><i class="fa fa-instagram" aria-hidden="true"></i></a><?php } ?>
    </div>

    <div class="copyrights">
        <div class="wrapper">
            <?php if(get_option('my_copyrights')) : ?>
                <?php echo get_option('my_copyrights');?>
            <?php else: ?>
                &copy <?php _e("2018 ALL RIGHTS RESERVED", 'wplian')?>
            <?php endif; ?>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>
</body>
</html>
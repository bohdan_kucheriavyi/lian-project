<?php get_header(); ?>
<?php get_template_part( 'template-parts/blog-inner-header'); ?>
<section class="blog_page">
    <div class="container">
        <div class="wrapper">
            <div class="blog_section">
                <div class="blog_content wow fadeInLeft">
                    <?php $current_page = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    $params = array(
                        'posts_per_page' => 6,
                        'paged'           => $current_page
                    );
                    query_posts($params);
                    $i = 0; ?>
                    <div class="grid">
                        <?php while(have_posts()): the_post();
                            $featured_img_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full'); ?>
                            <?php foreach((get_the_category()) as $category) { ?>
                                <?php if($i == 1 or $i == 2 or $i == 4) {?>
                                    <article id="post-<?php the_ID(); ?>" class="item_blog grid-item grid-item--height3">
                                        <a href="<?php the_permalink(); ?>">
                                            <img class="big_post_image" src="<?php echo $featured_img_url[0]; ?>" alt="big_post_image">
                                            <div class="post_info">
                                                <h3 class="post_title"><?php the_title(); ?></h3>
                                                <p class="post_author"><?php _e('by ', 'wplian'); the_author(); ?></p>
                                                <?php $text = get_the_content();
                                                $content = wp_trim_words( $text, 20, '...' );
                                                echo '<p class="post_desc story">'.$content.'</p>'; ?>
                                                <p class="post_date"><?php _e('on ', 'wplian'); echo get_the_date(); ?></p>
                                                <div class="post_social">
                                                    <a href="#"><i class="fa fa-share-alt" aria-hidden="true"></i></a>
                                                    <a href="#"><i class="fa fa-heart" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                        </a>
                                        <span class="blog_category">
                                            <?php echo $category->cat_name; ?>
                                    </span>
                                    </article>
                                <?php } else if($i == 3) {?>
                                    <article id="post-<?php the_ID(); ?>" class="item_blog grid-item grid-item--height2" style="background-image: url(<?php echo $featured_img_url[0]; ?>)">
                                        <a href="<?php the_permalink();?>">
                                            <span class="mask">
                                                <span class="play">
                                                    <i class="fa fa-play" aria-hidden="true"></i>
                                                </span>
                                            </span>
                                        </a>
                                    </article>
                                <?php } else {?>
                                    <article id="post-<?php the_ID(); ?>" class="item_blog grid-item blog-medium">
                                        <a href="<?php the_permalink(); ?>">
                                            <div class="post_info">
                                                <p class="blog_category"><?php echo $category->cat_name; ?></p>
                                                <h3 class="post_title"><?php the_title(); ?></h3>
                                                <p class="post_author"><?php _e('by ', 'wplian'); the_author(); ?></p>
                                                <?php $text = get_the_content();
                                                $content = wp_trim_words( $text, 20, '...' );
                                                echo '<p class="post_desc story">'.$content.'</p>'; ?>
                                                <p class="post_date"><?php _e('on ', 'wplian'); echo get_the_date(); ?></p>
                                                <div class="post_social">
                                                    <a href="#"><i class="fa fa-share-alt" aria-hidden="true"></i></a>
                                                    <a href="#"><i class="fa fa-heart" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                        </a>
                                    </article>
                                <?php } $i++;?>
                            <?php }  ?>
                        <?php endwhile; ?>
                    </div>
                    <?php if (  $wp_query->max_num_pages > 1 ) : ?>
                        <script>
                            var ajaxurl = '<?php echo site_url() ?>/wp-admin/admin-ajax.php';
                            var true_posts = '<?php echo serialize($wp_query->query_vars); ?>';
                            var current_page = <?php echo (get_query_var('paged')) ? get_query_var('paged') : 1; ?>;
                            var max_pages = '<?php echo $wp_query->max_num_pages; ?>';
                        </script>
                    <div class="loadmore">
                        <div id="true_loadmore"><?php _e('Load More', 'wplian') ?></div>
                    </div>
                    <?php endif; ?>
                </div>
                <div class="blog_widgets wow fadeInRight">
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>
